#!/usr/bin/env bash

echo "===Updateing Repositories==="
add-apt-repository -y ppa:ondrej/php
apt-get update

echo "===Instaling php7==="
apt-get -y install php7.1 php7.1-cli php7.1-fpm php7.1-curl php7.1-bcmath php7.1-json php7.1-mbstring php7.1-mcrypt php7.1-mysql php7.1-readline php7.1-sqlite3 php7.1-xml php7.1-zip php7.1-xsl libapache2-mod-php7.1 php7.1-gd php7.1-intl

echo "===Intaling Apache==="
apt-get install -y apache2

echo "===Intaling MySQL==="
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get install -y mysql-server-5.7	

echo "===Instaling git==="
apt-get install -y git

echo "===Instaling Composer==="
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

echo "===Intall and chmod Symfony==="
mkdir -p /usr/local/bin
curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
chmod a+x /usr/local/bin/symfony

echo "===Server conf==="
a2enmod rewrite
a2dissite 000-default.conf

if ! [ -L /var/www ]; then
	rm -rf /var/www
	ln -fs /vagrant /var/www
fi

cp /vagrant/vm_provision/hostfile /etc/apache2/sites-available/project.conf
a2ensite project.conf

apt-get -y autoremove

service apache2 restart

if [ ! -d /vagrant/project ]; then
	symfony new /vagrant/project lts
fi
